package ca.mcgill.sel.core.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.core.COREConfiguration;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREReuseConfiguration;
import ca.mcgill.sel.core.CoreFactory;

/**
 * This class provides helper methods to be handle {@link ca.mcgill.sel.core.COREConfiguration}s.
 *
 * @author CCamillieri
 */
public final class COREConfigurationUtil {

    /**
     * Prevent to instantiate.
     */
    private COREConfigurationUtil() {
    }

    /**
     * Get leaves features from a given configuration selection.
     * It looks at used {@link ca.mcgill.sel.core.COREConcernConfiguration} if any.
     * This method DOES not look into extended configurations, nor re-exposed features.
     *
     * The current implementation considers that the selection has no "hole",
     * ie considering the hierarchy of features A > B > C; if C is selected, A and B must be in the selected list
     * of the configuration as well.
     *
     * @param configuration - The configuration to check.
     * @return set of features that had no selected children.
     */
    public static Set<COREFeature> getSelectedLeaves(COREConfiguration configuration) {
        // Add selection from reused COREConcernConfiguaration
        Set<COREFeature> baseSelection = new HashSet<COREFeature>();
        baseSelection.addAll(configuration.getSelected());
        if (configuration instanceof COREReuseConfiguration
                && ((COREReuseConfiguration) configuration).getReusedConfiguration() != null) {
            baseSelection.addAll(((COREReuseConfiguration) configuration).getReusedConfiguration().getSelected());
        }
        Set<COREFeature> leaves = new HashSet<COREFeature>();
        // For each feature from the selection, check if one of its child is selected
        loop: for (COREFeature feature : baseSelection) {
            for (COREFeature child : feature.getChildren()) {
                if (baseSelection.contains(child)) {
                    continue loop;
                }
            }
            // If we're here, we didn't find any selected descendant
            leaves.add(feature);
        }
        return leaves;
    }

    /**
     * Make a basic merge of two configurations. Does not merge extended configurations.
     *
     * @param base - The base configuration.
     * @param toMerge - The configuration to merge
     */
    public static void mergeConfigurations(COREConfiguration base, COREConfiguration toMerge) {
        mergeConfigurations(base, toMerge, false, false);
    }

    /**
     * Make a basic merge of two configurations.
     *
     * @param base - The base configuration.
     * @param toMerge - The configuration to merge.
     * @param mergeExtended - Whether to merge extended configurations as well or not.
     * @param addComplete - Whether to add extended configurations that are already complete or not
     */
    public static void mergeConfigurations(COREConfiguration base, COREConfiguration toMerge,
            boolean mergeExtended, boolean addComplete) {
        List<COREFeature> selection = toMerge.getSelected();
        List<COREFeature> reExposition = toMerge.getReexposed();

        if (base == null || toMerge == null || base.getSource() != toMerge.getSource()
                || (base instanceof COREReuseConfiguration && toMerge instanceof COREReuseConfiguration
                && ((COREReuseConfiguration) base).getReuse() != ((COREReuseConfiguration) toMerge).getReuse())) {
            throw new IllegalArgumentException("Invalid configurations");
        }

        // merge selected features (union)
        for (COREFeature selected : selection) {
            if (!base.getSelected().contains(selected)) {
                base.getSelected().add(selected);
            }
        }
        // merge reExposed features (intersection)
        Set<COREFeature> deReExposed = new HashSet<COREFeature>();
        for (COREFeature exposed : base.getReexposed()) {
            if (!reExposition.contains(exposed)) {
                deReExposed.add(exposed);
            }
        }
        base.getReexposed().removeAll(deReExposed);

        // merge extended reuses
        if (mergeExtended) {
            loop: for (COREReuseConfiguration extended : toMerge.getExtendingConfigurations()) {
                for (COREReuseConfiguration baseExtended : base.getExtendingConfigurations()) {
                    if (baseExtended.getReuse() == extended.getReuse()) {
                        mergeConfigurations(baseExtended, extended);
                        break loop;
                    }
                }
                // If we're here, we did not have the reuse in our list, simply add it to the list.
                if (addComplete || !isConfigurationComplete(extended)) {
                    base.getExtendingConfigurations().add(EcoreUtil.copy(extended));
                }
            }
        }
    }

    /**
     * Check if a configuration is complete.
     *
     * @param configuration - The configuration to check.
     * @return true if there is no reExposed feature left in the selection.
     */
    public static boolean isConfigurationComplete(COREConfiguration configuration) {
        if (configuration == null) {
            return false;
        }
        for (COREReuseConfiguration extended : configuration.getExtendingConfigurations()) {
            if (!isConfigurationComplete(extended)) {
                return false;
            }
        }
        return configuration.getReexposed().isEmpty();
    }

    /**
     * Create a copy of a given configuration, and merges its reusedConfiguration in, if any.
     *
     * @param base - The configuration to merge.
     * @param mergeExtended - Whether to merge in extended configurations.
     * @param addComplete - Whether to add extended configurations if they are complete.
     * @return {@link COREReuseConfiguration} with same selection than the given one.
     */
    public static COREReuseConfiguration getConfigurationCopy(COREReuseConfiguration base, boolean mergeExtended,
            boolean addComplete) {
        COREReuseConfiguration configuration = CoreFactory.eINSTANCE.createCOREReuseConfiguration();
        configuration.setReuse(base.getReuse());
        configuration.setSource(base.getSource());
        configuration.setName(base.getName());
        if (base.getReusedConfiguration() != null) {
            configuration.getReexposed().addAll(base.getReusedConfiguration().getReexposed());
            configuration.getSelected().addAll(base.getReusedConfiguration().getSelected());
            mergeConfigurations(configuration, base, mergeExtended, addComplete);
        } else {
            configuration.getReexposed().addAll(base.getReexposed());
            configuration.getSelected().addAll(base.getSelected());
        }
        return configuration;
    }

    /**
     * Get configuration from a reuse by name.
     *
     * @param reuse The reuse to look into
     * @param name The name of the configuration to look for
     * @return The configuration with the given name
     */
    public static COREReuseConfiguration getConfigurationByName(COREReuse reuse, String name) {
        COREReuseConfiguration configuration = null;
        for (COREReuseConfiguration reuseConfiguration : reuse.getConfigurations()) {
            if (name.equals(reuseConfiguration.getName())) {
                configuration = reuseConfiguration;
            }
        }
        return configuration;
    }
}
