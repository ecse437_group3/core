package ca.mcgill.sel.core.util;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.commons.FileManagerUtil;
import ca.mcgill.sel.commons.StringUtil;
import ca.mcgill.sel.commons.emf.util.AdapterFactoryRegistry;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.commons.emf.util.ResourceManager;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREFeatureRelationshipType;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModel;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElement;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.CORENamedElement;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;

/**
 * Helper class with convenient static methods for working with CORE EMF model objects.
 *
 * @author mschoettle
 */
public final class COREModelUtil {

    private static final String REUSED_PREFIX = "Reused_";

    /**
     * Creates a new instance of {@link COREModelUtil}.
     */
    private COREModelUtil() {
        // suppress default constructor
    }

    /**
     * Creates a new {@link COREConcern} with an empty {@link COREFeatureModel} (one root feature).
     *
     * @param name the name of the concern
     * @return the newly created {@link COREConcern}
     */
    public static COREConcern createConcern(String name) {
        COREConcern concern = CoreFactory.eINSTANCE.createCOREConcern();
        concern.setName(name);

        // Create a Feature Model with a root feature.
        COREFeatureModel featureModel = CoreFactory.eINSTANCE.createCOREFeatureModel();

        COREFeature rootFeature = CoreFactory.eINSTANCE.createCOREFeature();
        rootFeature.setName(name);
        rootFeature.setParentRelationship(COREFeatureRelationshipType.NONE);

        featureModel.getFeatures().add(rootFeature);
        featureModel.setRoot(rootFeature);

        concern.setFeatureModel(featureModel);
        concern.getModels().add(featureModel);

        return concern;
    }
    
    /**
     * Get the local concern.
     * If the concern was not already reused, it creates a copy of the folder locally.
     * It then loads the local concern and returns it.
     * <b>Note:</b> The URIs to be provided are excepted to be file URIs.
     * I.e., {@link URI#createFileURI(String)} should be used instead of {@link URI#createURI(String)}.
     *
     * @param reusedConcernURI The file URI of the reused concern
     * @param reusingConcernURI The file URI of the concern reusing another concern
     *
     * @return The local concern
     * @throws IOException - Thrown if not able to copy the directory
     */
    public static COREConcern getLocalConcern(URI reusedConcernURI, URI reusingConcernURI) throws IOException {
        String sourceDirectory = reusedConcernURI.trimSegments(1).toFileString();
        String destinationDirectory = reusingConcernURI.trimSegments(1).toFileString();
        if (reusedConcernURI.path().equals(reusingConcernURI.path())) {
            // Concern cannot reuse itself
            return null;
        }
        
        // Make sure the concern name is decoded.
        String concernName = URI.decode(reusedConcernURI.trimSegments(1).lastSegment());
        String targetDirectory = destinationDirectory.concat(File.separator).concat(REUSED_PREFIX).concat(concernName);
    
        if (!(new File(targetDirectory).isDirectory())) {
            FileManagerUtil.copyDirectory(sourceDirectory, targetDirectory);
        }
        
        // Ensure the concern file name is decoded.
        String concernFileName = URI.decode(reusedConcernURI.lastSegment());
        String modelFileName = targetDirectory.concat(File.separator).concat(concernFileName);
        
        return (COREConcern) ResourceManager.loadModel(modelFileName);
    }

    /**
     * Creates a new {@link COREReuse}, reusing the given concern.
     *
     * @param reusingConcern the reusing concern
     * @param reusedConcern the referenced concern (external concern) of the {@link COREReuse}
     * 
     * @return reuse the COREReuse
     */
    public static COREReuse createReuse(COREConcern reusingConcern, COREConcern reusedConcern) {
        COREReuse reuse = CoreFactory.eINSTANCE.createCOREReuse();
        reuse.setReusedConcern(reusedConcern);
        
        Set<COREReuse> reuses = new HashSet<COREReuse>();
        
        for (COREFeature feature : reusingConcern.getFeatureModel().getFeatures()) {
            reuses.addAll(feature.getReuses());
        }
        
        String reuseName = COREModelUtil.createUniqueNameFromElements(reusedConcern.getName(), reuses);
        reuse.setName(reuseName);
        
        return reuse;
    }
    
    /**
     * Creates a new {@link COREModelReuse} with the given source and reuse.
     * 
     * @param source the external model that is reused
     * @param reuse the responsible {@link COREReuse} 
     * @return the {@link COREModelReuse}
     */
    public static COREModelReuse createModelReuse(COREModel source, COREReuse reuse) {
        COREModelReuse modelReuse = CoreFactory.eINSTANCE.createCOREModelReuse();
        modelReuse.setSource(source);
        modelReuse.setReuse(reuse);
        
        return modelReuse;
    }
    
    /**
     * Creates a new {@link COREModelExtension} with the given source.
     * 
     * @param source the external model that is extended
     * @return the {@link COREModelExtension}
     */
    public static COREModelExtension createModelExtension(COREModel source) {
        COREModelExtension modelExtension = CoreFactory.eINSTANCE.createCOREModelExtension();
        modelExtension.setSource(source);
        
        return modelExtension;
    }

    /**
     * Function called to delete the folder of a reused concern in the file system.
     * This should only on a concern that was previously obtained through CoreModelUtil.createConcernCopy.
     * Before attempting the deletion, the method checks if the concern is still used by a
     * reuse in the concern that made the reuse in the first place.
     *
     * @param concern - The concern whose directory we want to delete.
     * @param reusingConcern - The concern that created the folder we want to delete.
     * @return true if the directory was deleted, false otherwise.
     */
    public static boolean deleteReusedConcernDirectory(COREConcern concern, COREConcern reusingConcern) {
        File toDeleteDirectory = new File(concern.eResource().getURI().toFileString()).getParentFile();

        // Check for basic invalid parameters.
        if (concern == null || reusingConcern == null || EcoreUtil.equals(concern, reusingConcern)
                || concern.eResource() == null || !toDeleteDirectory.getName().startsWith(REUSED_PREFIX)) {
            return false;
        }

        // Check if the concern to delete is used somewhere in the reusingConcern.
        for (COREFeature feature : reusingConcern.getFeatureModel().getFeatures()) {
            for (COREReuse reuse : feature.getReuses()) {
                if (EcoreUtil.equals(reuse.getReusedConcern(), concern)) {
                    return false;
                }
            }
        }

        // We can delete the concern's folder.
        boolean success = false;
        try {
            FileManagerUtil.deleteFile(toDeleteDirectory, true);
            success = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return success;
    }

    /**
     * Get all the model reuses for a given {@link COREReuse} in a concern.
     *
     * @param coreReuse - The {@link COREReuse} to delete.
     * @return the set of modelReuses of this reuse.
     */
    public static Set<COREModelReuse> getModelReuses(COREReuse coreReuse) {
        COREConcern reusingConcern = (COREConcern) coreReuse.eContainer().eContainer().eContainer();
        return getModelReuses(reusingConcern, coreReuse);
    }

    /**
     * Get all the model reuses for a given {@link COREReuse} in a concern.
     *
     * @param concern - The concern that made the {@link COREReuse}.
     * @param coreReuse - The {@link COREReuse} to delete.
     * @return the set of modelReuses of this reuse.
     */
    public static Set<COREModelReuse> getModelReuses(COREConcern concern, COREReuse coreReuse) {
        Set<COREModelReuse> modelReuses = new HashSet<COREModelReuse>();
        if (concern == null || coreReuse == null) {
            return modelReuses;
        }
        
        for (COREModel model : concern.getModels()) {
            for (COREModelReuse modelReuse : model.getModelReuses()) {
                if (modelReuse.getReuse() == coreReuse) {
                    modelReuses.add(modelReuse);
                }
            }
        }
        
        return modelReuses;
    }
    
    /**
     * Get the {@link COREModelComposition} of the currentModel that contains mappings to a
     * specific (reused or extended) external {@link COREModel}.
     *
     * @param currentModel - The current {@link COREModel}
     * @param externalModel - The external {@link COREModel}
     * @return the {@link COREModelComposition} that refers to the external model, if any.
     */
    public static COREModelComposition getCOREModelComposition(COREModel currentModel, COREModel externalModel) {
        
        for (COREModelReuse cmr : currentModel.getModelReuses()) {
            if (cmr.getSource() == externalModel) {
                return cmr;
            }
        }
        for (COREModelExtension cme : currentModel.getModelExtensions()) {
            if (cme.getSource() == externalModel) {
                return cme;
            }           
        }
        return null;
    }
    
   /**
     * Determines whether modelElement is a reference or not by looking through all model extensions
     * and model reuses to find a COREMapping that is not a reference.
     *
     * @param modelElement the modelElement for which the check should be made
     * @return boolean whether or not the element is a reference
     */
    public static boolean isReference(COREModelElement modelElement) {
        /**
         * Due to a bug in Pivot OCL, templated properties are not supported currently (Eclipse Oxygen).
         * However, running the OCL derivation query a second time works just fine.
         * See https://bugs.eclipse.org/bugs/show_bug.cgi?id=492801#c24.
         */
        // TODO: Remove fix once bug in Pivot OCL is fixed 
        // TODO: Replace all calls with direct call to modelElement.isReference() once fixed.
        boolean result;
        
        try {
            result = modelElement.isReference();
            // CHECKSTYLE:IGNORE IllegalCatch: RuntimeException thrown by Pivot OCL due to bug.
        } catch (Exception e) {
            result = modelElement.isReference();            
        }
        return result;
    }
    
    /**
     * Collects a set of model extensions for models that are extended by the given model.
     * Collects these recursively in case an extended model also extends another model.
     *
     * @param model the current model
     * @return the set of model extensions for models that are extended this model
     */
    public static Set<COREModelExtension> collectModelExtensions(COREModel model) {
        Set<COREModelExtension> modelExtensions = new HashSet<>();
        
        collectModelExtensions(model, modelExtensions);
        
        return modelExtensions;
    }
    
    /**
     * Collects a set of model extensions for models that are extended by this model.
     * Collects these recursively in case an extended model also extends another model.
     * Model extensions are added to the provided set.
     * This means that duplicates, should they occur, are ignored.
     *
     * @param model the current model
     * @param extendedModels the current set of found model extensions
     */
    private static void collectModelExtensions(COREModel model, Set<COREModelExtension> extendedModels) {
        for (COREModelExtension modelExtension : model.getModelExtensions()) {
            // If it was already added, it is not necessary to search for extended models of that model again
            // this prevents infinite loops in case of cyclic dependencies between models.
            if (extendedModels.add(modelExtension)) {
                collectModelExtensions(modelExtension.getSource(), extendedModels);
            }
        }
    }
    
    /**
     * Collects a set of models that are extended by this model.
     * Collects these recursively in case an extended model also extends another model.
     * Only models of the same type as the given model are returned.
     *
     * @param model the current model
     * @param <T> the concrete type of the model (extending {@link COREModel}
     * @return the set of models that are extended by this model
     */
    public static <T extends COREModel> Set<T> collectExtendedModels(T model) {
        Set<COREModelExtension> modelExtensions = COREModelUtil.collectModelExtensions(model);
        Set<T> extendedModels = new HashSet<>();

        for (COREModelExtension modelExtension : modelExtensions) {
            
            /**
             * Ensure that the given model and the extension's source are the same.
             * 
             */
            if (modelExtension.getSource().eClass().isInstance(model)) {
                // Casting won't raise an exception due to Java's type erasure.
                // Could be enforced using: model.getClass().cast(modelExtension.getSource())
                @SuppressWarnings("unchecked")
                T extendedModel = (T) modelExtension.getSource();
                extendedModels.add(extendedModel);
            }
        }

        return extendedModels;
    }
    
    /**
     * Returns the set of model reuses of the model
     * and all its extended models in the hierarchy.
     *
     * @param model the {@link COREModel}
     * @return the set of model reuses
     */
    public static Set<COREModelReuse> collectAllModelReuses(COREModel model) {
        Set<COREModelReuse> modelReuses = new HashSet<>();
        
        Set<COREModel> extendedModels = collectExtendedModels(model);
        extendedModels.add(model);
        
        for (COREModel extendedModel : extendedModels) {
            modelReuses.addAll(extendedModel.getModelReuses());
        }
        
        return modelReuses;
    }
    
    /**
     * Returns all compositions for the given model. 
     * This includes all extensions as well as all model reuses amongst all extensions.
     * 
     * @param model the {@link COREModel}
     * @return the set of all model compositions
     */
    public static Set<COREModelComposition> collectAllModelCompositions(COREModel model) {
        Set<COREModelComposition> allCompositions = new HashSet<>();
        
        allCompositions.addAll(COREModelUtil.collectModelExtensions(model));
        allCompositions.addAll(COREModelUtil.collectAllModelReuses(model));
        
        return allCompositions;
    }
    
    /**
     * Collects all model elements that the given model element also is/represents.
     * A model element represents another element if it is mapped, therefore,
     * any property of the mapped from element is also accessible by the given element.
     * Note that the returned set includes the given element as well.
     * 
     * @param element the {@link COREModelElement} to collect all elements for
     * @return the set of {@link COREModelElement}s that the given element represents
     */
    public static Set<COREModelElement> collectModelElementsFor(COREModelElement element) {
        Set<COREModelElement> result = new HashSet<>();
        result.add(element);
        
        COREModel model = (COREModel) EcoreUtil.getRootContainer(element);
        
        collectModelElementsRecursive(result, model, element);        
        
        return result;
    }
    
    /**
     * Recursively collects all model elements that the given model element is/represents.
     * Recursively traverses the extension and reuse hierarchy to find all elements.
     * 
     * @param elements the current set of {@link COREModelElement}s
     * @param model the current {@link COREModel} to investigate
     * @param element the current {@link COREModelElement} to find corresponding elements for
     */
    private static void collectModelElementsRecursive(Set<COREModelElement> elements, COREModel model,
            COREModelElement element) {
        elements.add(element);
        
        Set<COREModelComposition> modelCompositions = new HashSet<COREModelComposition>();
        modelCompositions.addAll(model.getModelExtensions());
        modelCompositions.addAll(model.getModelReuses());

        for (COREModelComposition modelComposition : modelCompositions) {
            for (COREModelElementComposition<?> composition : modelComposition.getCompositions()) {
                COREMapping<?>  mapping = (COREMapping<?>) composition;
                if (mapping.getTo() == element) {
                    collectModelElementsRecursive(elements, modelComposition.getSource(), mapping.getFrom());
                }
            }
        }
    }
    
    /**
     * Filters the choice of values for the given object.
     * Removes all values that are not part of the same model
     * or any other model that is extended by the model of the given object.
     * 
     * @see #collectExtendedModels(COREModel)
     * @param object the object that contains a property with the choices
     * @param choiceOfValues the choice of values to be filtered
     * @return the filtered choice of values
     */
    public static Collection<?> filterExtendedChoiceOfValues(EObject object, Collection<?> choiceOfValues) {
        COREModel model = EMFModelUtil.getRootContainerOfType(object, CorePackage.Literals.CORE_MODEL);
        
        // Create a set of models where elements are allowed from.
        // Besides the current model, add extended models.
        Set<COREModel> models = COREModelUtil.collectExtendedModels(model);
        models.add(model);
        
        // Filter out all classes that are not contained in one of the possible aspects.
        // Also filter out all types that are not part of the current aspect.
        for (Iterator<?> iterator = choiceOfValues.iterator(); iterator.hasNext(); ) {
            EObject value = (EObject) iterator.next();

            // null is also contained in the list
            if (value != null) {
                EObject objectContainer = EcoreUtil.getRootContainer(value);

                if (!models.contains(objectContainer)) {
                    iterator.remove();
                } 
            }
        }

        return choiceOfValues;
    }
    
    /**
     * Filters out all elements from the result that are mapped to another element.
     *  
     * @param result the unfiltered result collection
     * @param compositions the set of {@link COREModelComposition}s to consider
     */
    public static void filterMappedElements(Collection<?> result, Set<? extends COREModelComposition> compositions) {
        for (COREModelComposition modelComposition : compositions) {
            for (COREModelElementComposition<?> composition : modelComposition.getCompositions()) {
                if (composition instanceof COREMapping<?>) {
                    COREMapping<?> mapping = (COREMapping<?>) composition;
                    result.remove(mapping.getFrom());
                    
                    TreeIterator<EObject> iterator = mapping.eAllContents();
                    while (iterator.hasNext()) {
                        COREMapping<?> nestedMapping = (COREMapping<?>) iterator.next();
                        
                        result.remove(nestedMapping.getFrom());
                    }
                }
            }
        }
    }

    /**
     * Generate a unique name based on the given name and the name of already existing elements.
     *
     * @param baseName - The default name to give.
     * @param elements - The elements to compare the name to.
     * @return The newly created name. Of form baseName{int}
     *
     * @param <T> The type of the elements to compare the name to.
     */
    public static <T extends CORENamedElement> String createUniqueNameFromElements(String baseName,
            Collection<T> elements) {
        Set<String> names = new HashSet<String>();

        for (CORENamedElement namedElement : elements) {
            names.add(namedElement.getName());
        }

        return StringUtil.createUniqueName(baseName, names);
    }

    /**
     * Function called to collect all models realized by the selected Features in the diagram.
     *
     * @param selectedFeatures The set of selected features
     * @param modelType The type of COREModel
     * @param <T> The type of COREModel
     * @return SetofModels
     */
    public static <T extends COREModel> Set<T> getRealizationModels(Set<COREFeature> selectedFeatures,
            EClassifier modelType) {
        Set<T> result = new HashSet<T>();
        for (COREFeature feature : selectedFeatures) {
            Collection<T> realizingModels = EMFModelUtil.collectElementsOfType(feature,
                    CorePackage.Literals.CORE_FEATURE__REALIZED_BY, modelType);
            loop: for (T model : realizingModels) {
                for (COREFeature realizedFeature : model.getRealizes()) {
                    if (!selectedFeatures.contains(realizedFeature)) {
                        continue loop;
                    }
                }
                result.add(model);
            }
        }
        return resolveConflicts(result);
    }

    /**
     * Resolves conflicts when there are models that realize more than
     * one feature. It loops through the selected models and finds for
     * each model if its set of realizing features includes the set of
     * realizing features for another model. If this is the case, it
     * removes the other model.
     *
     * @param selectedModels the selected features of the concern.
     * @param <T> The type of COREModel
     * @return set of selected models after resolving conflicts.
     */
    private static <T extends COREModel> Set<T> resolveConflicts(Set<T> selectedModels) {
        Set<T> resolvedModels = new HashSet<T>();
        outerloop: for (T selectedModel : selectedModels) {
            Iterator<T> iterator = resolvedModels.iterator();
            // Intermediate set used to not add to the set while iterating through it
            Set<T> intermediate = new HashSet<T>();
            while (iterator.hasNext()) {
                T resolvedModel = iterator.next();
                EList<COREFeature> selectedFeatures = selectedModel.getRealizes();
                EList<COREFeature> resolvedFeatures = resolvedModel.getRealizes();
                if (selectedFeatures.containsAll(resolvedFeatures)) {
                    iterator.remove();
                    intermediate.add(selectedModel);
                } else if (resolvedFeatures.containsAll(selectedFeatures)) {
                    continue outerloop;
                }
            }
            resolvedModels.addAll(intermediate);
            intermediate.clear();
            resolvedModels.add(selectedModel);
        }
        return resolvedModels;
    }
    
    /**
     * Unloads all external resources that the given model references.
     *
     * @param model the model that has references to external resources
     */
    public static void unloadExternalResources(COREModel model) {
        Set<COREModelComposition> modelCompositions = new HashSet<COREModelComposition>();
        modelCompositions.addAll(collectAllModelReuses(model));
        modelCompositions.addAll(collectModelExtensions(model));

        for (COREModelComposition modelComposition : modelCompositions) {
            unloadEObject(modelComposition.getSource());
        }
    }

    /**
     * Dispose the adapter factory for this EObject and unload his resource.
     *
     * @param objectToUnload The EObject to unload.
     */
    public static void unloadEObject(EObject objectToUnload) {
        if (objectToUnload != null) {
            AdapterFactoryRegistry.INSTANCE.disposeAdapterFactoryFor(objectToUnload);
            ResourceManager.unloadResource(objectToUnload.eResource());
        }
    }

}
