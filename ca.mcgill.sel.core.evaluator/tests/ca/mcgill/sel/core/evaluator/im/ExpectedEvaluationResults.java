package ca.mcgill.sel.core.evaluator.im;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 * Evaluation result class that gets the expected values from related csv files.
 * Expected results are defined in the corresponding .csv file for each .core file with the following format:
 * ModelName,ScaleFactorAsFloat,OffsetAsFloat
 * @author mduran35
 */
class ExpectedEvaluationResults {

    private Map<String, Map<String, float []>> table = new HashMap<>();
    private List<String> testCaseNames;
    
    /**
     * Initialize function that fills the table.
     * @param testModelPath is the path to the test cases.
     * @return true if initialization is successful.
     */
    boolean initialize(String testModelPath) {

        testCaseNames = new ArrayList<String>();
        String cvsSplitBy = ",";

        List<File> allFiles = new ArrayList<File>();
        Queue<File> dirs = new LinkedList<File>();
        dirs.add(new File(testModelPath));
        while (!dirs.isEmpty()) {
            for (File f : dirs.poll().listFiles()) {
                if (f.isDirectory()) {
                    dirs.add(f);
                } else if (f.isFile() && f.getName().endsWith(".csv")) {
                    allFiles.add(f);
                }
            }
        }

        for (File csvfile : allFiles) {

            HashMap<String, float []> valuesForTest = new HashMap<String, float[]>();
            Path csvFilePath = Paths.get(csvfile.getAbsolutePath());
            try {
                List<String> lines = Files.readAllLines(csvFilePath, Charset.defaultCharset());

                for (String line : lines) {
                    // use comma as separator
                    String[] testData = line.split(cvsSplitBy);
                    float[] scaleFactorAndOffset = {Float.parseFloat(testData[1]), Float.parseFloat(testData[2])};
                    valuesForTest.put(testData[0], scaleFactorAndOffset);
                }
                String testName = getTestNameFromFile(csvfile.getPath(), testModelPath);
                table.put(testName, valuesForTest);
                testCaseNames.add(testName);
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

        }
        return true;
    }

    /**
     * Extracts the name of the test from the path.
     * @param resultFile is the path of the test file.
     * @param testModelPath is the base path of the test cases.
     * @return the name of the test (e.g., Single_positive)
     */
    private String getTestNameFromFile(String resultFile, String testModelPath) {
        String testName = "";
        testName = resultFile.substring(resultFile.lastIndexOf(testModelPath) + testModelPath.length(),
                resultFile.lastIndexOf(".csv"));
        return testName;
    }

    /**
     * Function to get SF (scale factor) and OF (offset) values for an element in a test.
     * @param elementName name of the goal/feature in the model.
     * @param testName is the name of the test.
     * @return the array as {SF, OF}
     */
    float[] getResultsForElementInTest(String elementName, String testName) {
        return table.get(testName).get(elementName);
    }

    /**
     * Function to get SF (scale factor) value for an element in a test.
     * @param elementName name of the goal/feature in the model.
     * @param testName is the name of the test.
     * @return the scale factor.
     */
    float getScaleFactorForElementInTest(String elementName, String testName) {
        return table.get(testName).get(elementName)[0];
    }

    /**
     * Function to get OF (offset) value for an element in a test.
     * @param elementName name of the goal/feature in the model.
     * @param testName is the name of the test.
     * @return the offset.
     */
    float getOffsetForElementInTest(String elementName, String testName) {
        return table.get(testName).get(elementName)[1];
    }

    /**
     * Function that returns the list of names of test cases.
     * @return the array containing the list.
     */
    List<String> getTestCaseNames() {
        return testCaseNames;
    }
}
