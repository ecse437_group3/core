package ca.mcgill.sel.core.weaver.util;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.URI;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREModel;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.weaver.COREWeaver;

/**
 * Helper class with convenient static methods for working with the {@link COREWeaver}.
 *
 * @author cbensoussan
 */
public final class COREWeaverUtil {

    /**
     * The file extension for aspects.
     */
    public static final String ASPECT_FILE_EXTENSION = "ram";

    /**
     * String appended to woven aspect when saved.
     */
    public static final String WOVEN_PREFIX = "Woven_";

    /**
     * Creates a new instance of {@link COREWeaverUtil}.
     */
    private COREWeaverUtil() {
        // suppress default constructor
    }

    /**
     * Combines an aspect name based on the concern and the selected features.
     * The format is the following: "concernname"<"concatenated list of selected features">,
     * e.g., Authentication&lt;PasswordExpiry,Password&gt;
     * 
     * @param concern the reused concern
     * @param selectedFeatures the selected features of the concern
     * @return the combined aspect name
     */
    public static String createDefaultModelName(COREConcern concern, Set<COREModel> selectedFeatures) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(concern.getName());
        stringBuilder.append("<");

        for (COREModel realizingModel : selectedFeatures) {
            stringBuilder.append(realizingModel.getName());
            stringBuilder.append(",");
        }

        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        stringBuilder.append(">");

        return stringBuilder.toString();
    }

    /**
     * Create a String name for the given concern's woven aspect.
     * Will create a name of type:
     * Woven_{ConcernName}_{name of Woven Aspects from the concern}+_([ReuseName]_{Woven Aspects from the reuses})*
     *
     * @param concern - The concern that is being woven.
     * @param cwi - The {@link ConcernWeavingInfo} containing hierarchy of reuses and resolved aspects.
     * @return the string for this particular selection.
     */
    public static String createWovenConcernName(COREConcern concern, ConcernWeavingInfo cwi) {
        
        StringBuilder stringBuilder = new StringBuilder();

        // Concern name
        stringBuilder.append(WOVEN_PREFIX.concat(concern.getName()));

        // Woven aspects
        stringBuilder.append(getResolvedModelsToString(cwi));

        // Save the woven aspect in the Reused_ConcernName folder.
        URI fileURI = concern.eResource().getURI();
        fileURI = fileURI.trimSegments(1);
        fileURI = fileURI.appendSegment(stringBuilder.toString());
        fileURI = fileURI.appendFileExtension(ASPECT_FILE_EXTENSION);

        return fileURI.toFileString();
    }

    /**
     * Create an alphabetically-ordered string of form:
     * AspectA_AspectB_AspectC(_ReuseA_AspectA2_AspectB2_..)* based on the given {@link ConcernWeavingInfo}.
     * The name of each aspect is the name of its serialized file.
     *
     * @param cwi - The {@link ConcernWeavingInfo} containing hierarchy of reuses and resolved aspects.
     * @return the string for this particular selection.
     */
    private static String getResolvedModelsToString(ConcernWeavingInfo cwi) {
        List<COREModel> resolvedModel = cwi.getWovenAspects();

        StringBuilder stringBuilder = new StringBuilder();
        String[] selectedModelsName = new String[resolvedModel.size()];

        int i = 0;
        for (COREModel model : resolvedModel) {
            // Use file name instead of aspect name to avoid problems with invalid characters.
            selectedModelsName[i++] = model.eResource().getURI().trimFileExtension().lastSegment();
        }
        Arrays.sort(selectedModelsName);

        for (i = 0; i < selectedModelsName.length; i++) {
            stringBuilder.append("_");
            stringBuilder.append(selectedModelsName[i]);
        }

        for (COREReuse reuse : cwi.getChildrenWeavingInfo().keySet()) {
            stringBuilder.append("_" + reuse.getName());
            stringBuilder.append(getResolvedModelsToString(cwi.getChildrenWeavingInfo().get(reuse)));
        }

        return stringBuilder.toString();
    }
}
